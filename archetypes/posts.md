---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
image: "/images/post/{{.Name}}.jpg"
author: "Iván Sánchez Milara"
description: ""
categories: [""]
type: "post"
---