---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
image: "/images/post/{{.Name}}.jpg"
author: ""
description: ""
categories: [""]
type: "post"
---