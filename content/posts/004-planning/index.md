---
title: "Pedagogical Competence. Session I. Planning"
date: 2024-04-25T10:27:00+03:00
draft: false
image: ""
author: "Iván Sánchez Milara"
description: ""
categories: ["Reflection", "Seminar","Pedagogical Competence I"]
type: "post"
---

## Introduction

Planning is a critical task for any teaching activity. When we are talking about planning in education we can refer to multiple levels, it can be plan a curriculum for an area or a whole country, a curriculum for a degree program, a plan a complete course or plan a single class (See Figure 1). In each one of the cases the approach of planning is different. 

{{< figure src="/posts/004-planning/images/004_1.jpg" caption="Figure 1. Different levels of planning" >}}

The process of planning a curriculum or a degree program is out of the scope of this post. In any case, this is related to the concept of _Educational Planning_ which is the process of "_of setting out in advance, strategies, policies, procedures, programmes and curriculums or standards through which an educational objective or set can be achieved_"[^1]

Let's focus more in the _Instructional Planning_ or _[Instructional Design](https://en.wikipedia.org/wiki/Instructional_design)_ . This concept includes the planning of a course or a lesson. In both cases it is necessary to have multiple things into consideration including Learning outcomes, Assessment activities, instruction methodologies, pedagogical models,  context, state and need of the learner. There are many frameworks available for developing courses. Perhaps one of the most utilized nowadays is [ADDIE Model](https://en.wikipedia.org/wiki/ADDIE_Model) explained in big detail in [1]. ADDIE Model consist of five phases namely: _Analysis phase_ where the context, pedagogical considerations, learning theories are analyzed, _Design phase_ where the learning outcomes, the assessment instruments and instructional methods are chosen, the _Development phase_ in which the content is created, _Implementation phase_ in which the course is taught and _Evaluation phase_ in which the course is evaluated (e.g. did most students achieved learning outcomes, why or why not). In the [section where I discuss about the Pedagogical Script](#pedagogical-script), I would go in a little bit more in detail on the Bloom taxonomy that can be an interesting model to use in the planning phase. 

If we forget about technicities, Figure 2 and Figure 3 presents in layman's language aspects that are important to consider when planning a whole course [^2] or a single class session [^3].

{{< figure src="/posts/004-planning/images/004_2.jpg" caption="Figure 2. What aspects should you consider to prepare a course" >}}

{{< figure src="/posts/004-planning/images/004_3.jpg" caption="Figure 3. What aspects should you consider to prepare an individual lesson" >}}

In my opinion, planning is a very important step, that many times, specially in novice teachers, is neglected. Many times the focus of planning is in preparing the content and material, forgetting other important aspects such assessment (e.g formative assessment) or even learning outcomes. I have experienced this in myself. When I started working as a teacher, planning was merely creating slides to present students. Little by little, I have realized the importance of including other aspects such as pedagogical models, different methodologies, and perhaps more important a clear assessment, that might even guide how you create the content and instructional methods (see [cognitive alignment](#pedagogical-script) below). Before any class, I usually write a lesson plan, similar to the one I defined for [my lecture in this topic](/files/08.09.2021_Lesson_Plan_Ivan.pdf). 

The session about planning took place online on 08.09.2021 in Teams. I was in group 1 with Eirini, Virginia, Afnan and Mahmood.


## Main learnings

### Official documents and regulations and planning {#regulations}

When you are planning a course or a lesson you should have into consideration a wide set of laws and regulations going from European Union directives, Finnish government laws, Ministry of Culture regulations or recommendations to local (municipal) regulations and even institutions guidelines. Depending on the level of education you are working in regulations are different.  

For instance in the case of comprehensive education, in Finland, all schools in the country have the same curriculum ([National Core Curriculum for Higher Education](https://www.oph.fi/sites/default/files/documents/new-national-core-curriculum-for-basic-education.pdf)), where the learning objectives of each one of the different courses for all levels are defined. Schools and teachers have freedom in how to instruct this content. However, the new Finnish curriculum emphasize transversal competences / skills (skills that can be utilized in different fields or areas) and emphasize the utilization of Phenomenon Based Learning as pedagogical model. National curriculum is defined by the [Finnish National Agency for Education](https://www.oph.fi/en). 

If we focus more in the Vocational education and the Higher Education the situation is different. Neither VET or Higher Education has a proper curriculum but vocational schools and universities can define their own curriculum. 

In the case of VET in Finland [2] the Finnish National Agency for education helps funding education and training projects, while the Ministry of Education and culture grants licenses to provide vocational education which determines the task of the educational provider and give permission to provide qualification, but is the education provider which decides how organize the education they offer. So vocational schools create their own curriculum and regulations that any teacher must follow when creating a course or a program.  Vocational education in Finland is designed both for young people without upper secondary qualifications and for adults already in work life, with focus in working life skills [^4] and individual learning pathways. In addition VET must be free in Finland. VET in Finland was recently reformed [^5] and is regulated by the [**Act on Vocational Education and Training** (531/2017; Ammendments up to 547/2018)](https://finlex.fi/en/laki/kaannokset/2017/en20170531.pdf)[^6].

On the other hand, the Ministry of Education is responsible for the "_planning and implementation of higher education and science policy and preparing statutes, national Budget proposals and Government decisions that apply to these_" [^7]. The ministry of Education grants permission for the creation of new universities and new degrees inside the universities, but after that, universities (both universities of applied science and research  universities) has autonomy on how to organize their education, including curriculum and the creation of Master Degree Programs. The universities are responsible for the content and quality of education, and decides on students admissions. In the case of higher education the need also to follow European rules such as the [ECTS system](https://ec.europa.eu/education/resources-and-tools/european-credit-transfer-and-accumulation-system-ects_en). The University of Applied Sciences are regulated by the **[Universities of Applied Sciences Act](https://www.finlex.fi/en/laki/kaannokset/2014/en20140932_20160563.pdf)**[^8]. As in Vocational Schools, the universities create their own regulations, following always the rules created by the ministry of Education. 

In the case of research Universities, the law that regulates them is the **Universities Act** [^9] Some aspects that regulate this act is the type of degrees that universities can provide (bachelor, master, Phd and other studies), the tuition languages and communication, tuition fees, administrative organization of universities (Board, rector and collegium), students admission process, assessment of study attainments, disciplinary actions and university financing. In addition, each university define its own regulations regarding education, always under the frame of the Universities Act [^9]. [University of Oulu Education Regulations](https://www.oulu.fi/external/education_regulations.pdf). For planning a course is important to go through this regulation because it defines for instance the obligations of a teacher, who define and approve the curriculum of a program (Program Director in the first case and vicerector of education + Education dean + faculty dean in the second case), the right of students to receive a proper and structured student counselling, the evaluation and assessment of study attainments, possibility of retaking study attainments, use of foreign languages ... For instance, in my case was important when I have to prepare the assessment guidelines for my courses. In general, if course is based on a final exam, I should offer to additional opportunities to students to pass the exam. However, in the case of other evaluation methods, this rule do not apply. Most of my courses are evaluated through exercises and projects, hence I do not have to consider this rule. 

The process and responsabilities to define a program and a course curriculum is clearly explained in our intranet. Every year, we have to check if the actual information about the course (description, learning outcomes, content...) is up to date. We have also to define the different learning activities and the amount of time of each one, so education planners could make proper schedule for all the courses in the different degree programs. The phases of curriculum development of a degree program, as stated in the intranet, is as follows: 


- _Responsibility Allocation:_ The programme director leads the curriculum planning process, establishing responsibilities among the faculty, the degree programme, and the Academic Affairs Service Team.
- _Needs Analysis:_ Development needs are identified based on an analysis of previous year's data (e.g., pass rates, student numbers) and student feedback.
- _Regulatory Acquaintance:_ All parties get acquainted with regulations, goals, instructions, and schedules governing curriculum planning. The University of Oulu's "Annual Wheel" outlines key dates and milestones for curriculum development, addressing administrative, marketing, and bureaucratic aspects.
- _Competence Analysis:_ The future working life competence needs are identified, guiding necessary curriculum changes to enhance working life relevance.
- _Goal Setting and Development Areas:_ Goals are set, focusing on internationalization and the integration of entrepreneurial content, and working life cooperation. This includes remote study options, collaborations with other institutions, and emphasizing an entrepreneurial mindset.
- _Partnership Identification:_ Potential partners and cross-institutional study networks are identified, and negotiations with these partners are initiated.
- _Learning Outcomes and Programme Description:_ The programme director establishes the intended learning outcomes and prepares the programme description, which is made public on the StudyInfo portal and other relevant advertising platforms.
- _Degree Structure and Timing Plan:_ The programme director prepares and seeks approval for the degree structure and timing plan from the Faculty Education Dean. This includes details on specific courses and their modalities.
- _Course Responsibility Assignment:_ The programme director appoints persons in charge of each course in the degree program.
- _Course Development:_ The person in charge of each course defines the learning goals, content, teaching methodologies, assessment criteria, and implementation details, aligning with the programme director's guidelines. They also ensure the accuracy of course information in the university study guide.

 
When dealing with all the different rules for running courses and programs, the biggest challenge is knowing who to ask for help or where to find the required information. There are so many regulations covering everything from local to international requirements that it’s very difficult for one person to know them all. Nowadays more of the information for our university is in the intranet database. But, right now, the information is so vast, that is difficult to navigate, unless someone help you how to start.   

#### Links to different important organisms and sites: 

* [European commission. ECTS Guide](https://ec.europa.eu/education/resources-and-tools/european-credit-transfer-and-accumulation-system-ects_en)
* [Finnish National Agency for Education](https://www.oph.fi/en): Develop Finnish compulsory education and training., early childhood education and lifelong learning. They also promote internationalization of Finnish education. 
* [Finnish Ministry of Education and Culture](https://minedu.fi/en/):  Responsible for the development of education, science, cultural, sport and youth policies, and for international cooperation in these fields. It includes legislation and budgeting.
* [Finlex data Bank](https://finlex.fi/en/): Online database of legislative information in Finland. It includes all Finish acts and decrees. Some of them can be found also in English. 

### Views of learning and planning

When planning either a curriculum, course or individual class / workshop / learning activity, teacher must consider the context, group preferences and characteristics and of course, preferences of the teacher. In the end, a teacher might feel more comfortable using some methodologies than others, and it is important to provide teacher freedom so he/she chooses the option that considers optimal to get maximum from students given the educational content. 

In this section I am not discussing about different teaching methodologies or pedagogical frameworks (this will be something developed in future posts) but just aspects related to planning and learning. 

During the discussion in the teaching session, two bigs approaches for the planning were identified. Although I do not like that much this division (at least with what respects to the terminology), because it looks like the traditional planning based on content is not effective, but it is how it appeared during the discussion. In general, I believe that is always better to use a blended approach, including the best of both approaches, and adapt to the education context. In education one-solution-fits-to-all does not exist. 

1. **Traditional planning based on content**
This planning is based on the traditional transfer of information from teacher to student, in which the learning happens as atomic increases in knowledge. The goal is that students get uniform understanding of concepts, facts or procedures that are fundamental in the subject. The planning of the activity is done by teacher herself, without considering input from students. Usually, plans are linear and rigid without much space for modification. Content is usually divided in topics or chapters that built upon knowledge acquired in previous ones. 

    This type of approach usually follows a teacher centric approach with the associate challenges: difficult to motivate and engage students, rigid framewok for adapting to learners dynamic needs and strong emphasis on coverage ("teach all content"). However, the linear and structured approach might help some learners that need a clear structure of the subject. Furthermore, might be useful, when it is necessary to provide large amount of material in limited amount of time. The structured and linear approach, enables easy assessment (including continous assessment) in which the progression of the student's learning in the topic can be easily evaluated at regular intervals.  

2. **Learning based planning** 
This planning focus more on the learning process than in the content itself. The goal is to that student gain learning by developing its own comprehension. In this type of planning [^10], students can participate in the planning part, for instance providing information on which topics might be more interesting for them. Planning are not much detailed and are flexible. They can be adapted depending on the context, kids interest and needs in the classroom. 

    This planning approach usually follows a student-centered approach, in which the planning takes into account students interest, prior knowledge and skills levels. Focus is not that much on the content but in the acquisition of skills and competence. The goals are not framed in terms of acquisition of factual knowledge but in the competencies that student should acquire. Those competences, usually includes also side competences such us problem-solving, critical thinking, creativity ... Tasks tried to be linked to real world problems, to make them more relevant and foster engagement. Although this type of learning increase students' engagement, permits more personalization of the learning and enables dynamic change in the instruction based on teachers' observation and results of formative assessment. However, does not come wihtout disadvantages such as time-consuming for planning (there is a need of understanding current situation of the whole group and each individual student), time-consuming for execution (tasks usually require more time than traditional lectures) and assessment complexity (measuring competences or skills is not as straight forard as testing factual knowledge). 

I found [this activity](/files/student-centered-approach.pdf) that guides on how to build a student-centered plan to support learning differences. It provides guided instructions, research, resources and even assessmente criteria for the activity. 

My plannings tend to blend both approaches. For instance in the  [Computer Systems Course](https://lovelace.oulu.fi/tietokonej%C3%A4rjestelm%C3%A4t/tietokonej%C3%A4rjestelm%C3%A4t/) I am currently teaching, I use content-based planning to structure my lectures where I explain specific computer architecture and programming concepts, while the learning based approach is given by a guided project in which students design and implement a program in which they need to put into practice all knowledge provided during the class. In this second part I assess also aspects such as problem solving, computational thinking and team work. 

#### Learning approaches {#learning-approaches}

When planning a education program, a course or an activity. It is important to consider which are the learning approaches that better adapt to the educational context. In this section we are discussing three approaches that have a strong impact on the planning of the activity. I am not including teacher-centric approach, because it is perhaps the one that we all know from school. I will focus in three other approaches namely: student centered learning, personalization and competence-based education.

* *Student centered learning* 
    In this approach teacher uses different approaches to **facilitate** the learning to a student that takes an active role in the learning process. The learner is not just listening teacher explanations but he/she is actively involved in the learning, taking even decicisions on what to learn and how to learn it. The teacher should be able to engage student in the learning process giving them some level of control. Teacher should also help learners to activate a meta-cognition process: reflect on what they have learned and how they are learning it. 

    In general, we can identify three main foundations in student-centered learning: 
    * Active learning: Students take active role in the process, either solving problems, answering questions, discussing with other colleagues, brainstorming and generating ideas ...
    * Cooperative learning: In this context, social learning is also important. Other colleagues are also an indispensable part in the learning process, helping to understand concepts and skills. However, it is still important to ensure positive interdependence and individual accountability. 
    * Inductive learning: Students learn by solving particular challenges until reaching a generalization. 

    There are multiple methodologies supporting student centered learning such as: inquiry based learning, case-based instruction, problem-based learning, project-based learning, just-in-time teaching... Some of these methodologies will be covered in future posts.

* *Personalization*
    A personalized learning is the customization and adaptation of educational methods and techniques so that the learning process is better suited for each individual learner, with their own unique learning style, background, needs, and previous experiences. This helps to meet students expectations for instance related to relationship with other stakeholders (student is  not a single number among the multitude), relevance of the learning to student's own context, learning to her/his own pace and in the order that suits better their skills/interest, have a choice on what and how to learn or having real challenge that motivates him/her

    In this approach, material, content, methodologies, processes, scheduling or assessment is adapted and personalized to each student. To achieve this is very important to know very well to student.

    One field in which personalized learning has big importance is in corporate training, in which the higher level of expertise and specialization many times require a high degree of personalization. There are lot of tools/forms available that helps teachers to identify different aspects of the student and help to create a personalized educational path for the student.  [This form](/files/edu-personalized-learning-sample-template.pdf) (from which I cannot remember the original source) is a good example of such content. 

    There is lot of talk on how AI can support personalized learning, and it is always a use-case in most of demos for the most recent Large Language Models that has been presented in the last two years. However, still there is no serious research on the topic. 

* *Competence based learning*
    This approach focus on acquiring set of individual competencies. It does not present a classical course structure, but learning build upon competences / skills. In this case, the core is not a course content that students must learn, but a set of competence that student must master. Competence is defined by the [Cambridge dictionary as](https://dictionary.cambridge.org/dictionary/english/competence) *"the ability to do something well"* and perhaps more in-line of what we are looking for, by the [Merrian Webster  as](https://www.merriam-webster.com/dictionary/competence) *"he quality or state of having sufficient knowledge, judgment, skill, or strength (as for a particular duty or in a particular respect)"*

    [FabAcademy](https://fabacademy.org/) a global educational program on digital fabrication is a good example on how to implement competence based learning. In this program students learn different digital fabrication processes that can be executed in a Fab Lab or other learning space. Each week students are given a set of competences that student must learn (e.g. *"Identify and explain processes involved in using the laser cutter."* ) However, the software and hardware tools depends on the students (and availability of the fablab). With the help of instructors student can explore different processes related to the laser cutter until he/she is able to explain the whole process to others. 

    In this case, it is important to guide the learner and help her/him to build a personal pathway. Hence following aspects should be carefully taking into consideration when planning course / activities using competence based learning : 
    * Provide clear and specific learning targets, including comprehensible explanation on what students should know and be able to do in order to advance and earn credit. Learning target and performance indicator (if any) should be clear identified. The teacher should also provide information on how student can demonstrate his/her level of proficiency in such competency. 
    * Provide continous assessment, support, and monitoring of individual students’ progress as they work toward meeting these targets.
    * Student must demonstrate mastery of competencies before they earn credit and advance to a new competence.
    * Provide flexible pacing for students who are ready to move more quickly or who need more time to achieve competency

    Competence based learning comes with multiple barriers: 
    * Ambiguity in defining measurable and observable competences
    * Difficulty in assessing the skills in real world scenarios. Performance indicator are many time not well defined. Could exist a mismatch between teacher and student understanding on the necessary level of competence and how to demonstrate it. 
    * Balence between field knowledge and skills: Overemphasizing on skills might lead to gaps in some foundational theoretical specific field knowledge.
    * Resource intensive: high level of personalization and continous monitoring consumes lot of teacher's resources
    * Resistance from other colleagues or administration, among other aspects, because it is sometimes difficult to align it with traditional educational systems. 

### Instructional design and pedagogical script. {#pedagogical-script}

One important aspect of planning process is scaffolding [3] learning. But before that, we might define what is learning. David A Kolb (the popular american psycologist author of the popular [experiential learning cycle](https://www.researchgate.net/profile/David-Kolb-2/publication/303446688_The_Kolb_Learning_Style_Inventory_40_Guide_to_Theory_Psychometrics_Research_Applications/links/57437c4c08ae9f741b3a1a58/The-Kolb-Learning-Style-Inventory-40-Guide-to-Theory-Psychometrics-Research-Applications.pdf) [4]) defines learning as *"the process whereby knowledge is created through the transformation of experience"*. Many theories tries to explain how learning actually happen (what will be covered in future posts).

For scaffolding learning and achieve learning goals it is recommend to prepare a kind of guide to orchestrate teaching and learning processes. This guide is known as **pedagogical script**. Although this concept seems to be taken from Computer-Supported Collaborative learning field [^11] it can be generalized to any context. 

The pedagogical script defines what happens, when it happens and how it happens in the learning process, ensuring a natural instructional flow. In definitive, the script aims to organize the interaction betwen teachers, students and educational material. 

The pedgagogical script includes learning objectives, roles of participants, educational activities (tasks, exercises, interactions ...) that facilitaes the learning such as individual assignment, group discussion, reflection journals..., **sequence (chronological or logical order in which activities are going to be executed)**, resources and tools, and assessment and feedback. My understanding from pedagogical script is that the main focus is in the sequence of activities, since the other aspects are covered more generally by instructional design. 

{{< figure src="/posts/004-planning/images/004_4.png" caption="Figure 4. A pedgagogical script, integrating different learning processes with teaching processes." >}}

{{< figure src="/posts/004-planning/images/004_5.png" caption="Figure 5. Steps to build a pedagogical script. [Original source.](https://teachingacademy.concordia.ca/courses/course-design/lessons/instructional-planning/)" >}}

I found a very extensive [handbook with step-by-step guidelines on how to plan a course / lesson using instructional planning](https://teachingacademy.concordia.ca/courses/course-design/lessons/instructional-planning/). In the end creating a pedagogical script is an important part when defining the instructional flow when planning a course or a lesson. In this document, the section related to [instructional flow](https://teachingacademy.concordia.ca/courses/course-design/lessons/instructional-planning/topic/instructional-flow/) is the most relevant related to pedagogical script. 

In the end the instructional design must be able to align the learning goals of a course or unit, teaching and learning methodologies and  target for the assessment of student performance. At the same time, as constructivism theories highlight, the student must be able to build meaning out of any learning activity, hence it is crucial a second alignment: the alignment between the instructional design with the learner’s cognitive level and capacity for processing and understanding information. This marriage between construvism and instructional design theories is what John Biggs named **"Constructive alignment"** [5]

Since, at this moment I have not covered yet learning theories, I  focus in the first type of alignment (the one known also as **cognitive alignment**), that is alignment between learning objectives, instructional activitis and assessement. Cognitive alignment is often guided by **Bloom Taxonomy** a framework for classifying statements of what is expected students to learn as a result of a learning activity. It categorize the cognitive tasks in six different levels, presented in the following table. 

| **Level**        | **Description**                                                    | **Synonyms**                                  |
|------------------|--------------------------------------------------------------------|------------------------------------------------------------------------|
| **Remembering**  | Recall basic facts, concepts, or answers from memory.              | Identify, Recognize, Recall, List, Define, Repeat, Name, State.        |
| **Understanding**| Explain ideas or concepts; demonstrate comprehension.             | Explain, Describe, Summarize, Paraphrase, Interpret, Discuss, Classify.|
| **Applying**     | Use knowledge in new situations or solve practical problems.       | Use, Implement, Apply, Demonstrate, Solve, Execute, Illustrate, Operate.|
| **Analyzing**    | Break down information into parts to explore relationships.        | Analyze, Compare, Differentiate, Examine, Test, Organize, Outline.     |
| **Evaluating**   | Make judgments based on criteria and standards.                    | Evaluate, Judge, Argue, Defend, Assess, Critique, Recommend, Justify.  |
| **Creating**     | Produce something new or original by combining knowledge and skills.| Create, Design, Construct, Develop, Formulate, Plan, Invent, Generate. |

The teaching strategies should align with the desired level of cognitive engagement expected in the learning outcomes. For instance, if the idea is that student is just able to list the colors in the rainbow (remembering), it is not necessary that he is able to apply the light theory in order to build a prism (creating) to show the colors. 

In my course, I try to heavily use Bloom taxonomy when defining the learning goals of the course. For instance, in an introductory course on embedded programming these are the main learning outcomes that I define for the introductory lecture: 

> * Being able to define most important concepts related to embedded programming such as microcontrollers, sensors, and actuators.
> * List different types of sensor and actuators.
> * Name different microcontroller family boards and identify different components.
> * Explain what input/output in a microcontroller is and how pins work.
> * Explain the main characteristics of the Raspberry Pi Pico.
> * Set up the Arduino development environment for programming. 

In this case, being an introductory course, means most of the learning outcomes belong to the remembering and understanding level. In the next lecture, learning outcomes focus more on the applying, evaluation and even creating part (implement own code)

## Additional content

[Lecture on planning, teaching and guiding](/files/planning_teaching_guiding_ivan.pptx). Iván Sánchez

## Bibliography
[1]  Morrison, Gary R. Designing Effective Instruction, 6th Edition. John Wiley & Sons, 2010.

[2]  Finnish National Agency for Education. Finnish VET in a Nutshell. Link: https://www.oph.fi/sites/default/files/documents/finnish-vet-in-a-nutshell.pdf. Last accessed 01.10.2021

[3] van de Pol, J., Volman, M. & Beishuizen, J. Scaffolding in Teacher–Student Interaction: A Decade of Research. Educ Psychol Rev 22, 271–296 (2010). https://doi.org/10.1007/s10648-010-9127-6

[4]  Kolb, David A. (2015) [1984]. Experiential learning: experience as the source of learning and development (2nd ed.). Upper Saddle River, NJ: Pearson Education. ISBN 9780133892406. OCLC 909815841.

[5] Biggs, J. (1996). Enhancing teaching through constructive alignment. Higher education, 32(3), 347-364.

[6] Krathwohl, D. R. "A Revision Bloom's Taxonomy: An Overview." Theory into Practice (2002).

## Footnotes
[^1]: https://www.slideshare.net/shafiqurrehman526/educational-planning-56831868 "Educational planning"
[^2]: https://www.uts.edu.au/research-and-teaching/learning-and-teaching/enhancing/planning-and-preparing-teaching "Planning a course"
[^3]: https://crlt.umich.edu/gsis/p2_5 "Planning a class"
[^4]: https://minedu.fi/en/vocational-education-and-training "Ministry of Education of Finland. Vocational Education. "
[^5]: https://minedu.fi/en/reform-of-vocational-upper-secondary-education "Reform of Finnish Vocational Education. "
[^6]: https://finlex.fi/en/laki/kaannokset/2017/en20170531.pdf "Act on Vocational Education and training"
[^7]: https://minedu.fi/en/higher-education-and-research "Ministry of Education. Higher Education"
[^8]: Universities of Applied Sciences Act "Universities of Applied Science Act"
[^9]: https://www.finlex.fi/fi/laki/kaannokset/2009/en20090558.pdf "Universities Act"
[^10]: https://www.edutopia.org/article/student-centered-planning "Student centered planning"
[^11]: https://jyx.jyu.fi/bitstream/handle/123456789/37597/978-951-39-3443-9.pdf?sequence=1&isAllowed=y "Pedagogical scripts to facilitate CSCL"