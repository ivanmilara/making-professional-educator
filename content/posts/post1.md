---
date: 2021-09-06T12:30:56+03:00
title: "Hello World!"
image: "/images/post/post-1.jpg"
author: "Iván Sánchez Milara"
description: "Hello World"
categories: ["test", "blog"]
type: "post"
---

This is my first post!!!!

 I have used a static website generator named [Hugo](https://gohugo.io/) and a template authored by [Northenlab](https://gethugothemes.com/products/northendlab/) for creating this blog. The blog is hosted in [Gitlab](https://about.gitlab.com/) I will explained in a future post why I chosen this setup. 

Still there are somethings that I need to tune (e.g. add support for comments) but let's go step, by step. 