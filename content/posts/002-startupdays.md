---
title: "Start-up days (I). Introduction"
date: 2021-09-28T10:46:41+03:00
draft: false
image: "/images/post/002-startupdays.jpg"
author: "Iván Sánchez Milara"
description: "Reflection on the startup days of the Professional Teacher Education course"
categories: ["Reflection", "Seminar","Orientation"]
type: "post"
---

Let's start the serious part of the blog. In this post I would like to give my impressions of the first three days of the [Professional Teacher Education Course at OAMK](https://www.oamk.fi/opinto-opas/en/school-professional-teacher-education) (25th - 27th August 2021). Actually, I have to say that it was a really interesting experience. It has been few years since I do not participate in a long course. In the last years, I have sat in  some courses and workshops, but always being short courses that did not required large after-class work. I think the last time I followed a course that would require that much time from me was [FabAcademy](https://fabacademy.org/). But still, I felt before starting that the effort would worth it: first I expected to get new tools for my teaching duties and secondly it would expand my job careers possibilities. I have always liked teaching, and I have been teaching in many different environments and context, but I have never got a proper certification. 

 Furthermore, it was the first time since COVID pandemic started, that I could attend a face-to-face event with more than five people in the same room. Sincerely, I was a little bit tired of discussing in front of a computer. I think you missed a lot of information, since the body language of people is also very important. In addition, I think people are more active when they are in face-to-face discussion. Besides, I do not know if it is just me, but I tend to distract myself, and loose concentration much more often when I am in an online meeting than when I am face-to-face.

 Still, the three full-days of class felt really exhausting. I do not know if it is the age, or maybe I am not used anymore to that long working sessions, in which you should keep concentration almost the full time. I have to admit that the last day I ended completely exhausted, but at the same time in good spirit because I found discussion and future activities very promising. 

 In addition to the introductory sessions on 1st September we were through a technical test, where we went through Teams and we tested how we can use online tools such as Google Drive or Microsoft 365 to collaborate online and create group content. 

 Teachers were Janne Länsitie, Riikka Barber and Kimmo Kuortti (who unfortunately would not continue with us after the three startup sessions)

 ## Sessions content

The sessions were mainly structured so we could understand the learning approach that we will be following during the rest course: methodologies, tools and assessment. In addition to that, we start to know each other and work in the hybrid mode (some students in local class, while others connected remotely in Zoom). The three days were organized more or less as follows (I focus in content, and not that much in the order that the different aspects were treated)

_25th August_
* Get to know each other. Who are we and which are our expectations?
* Education system in Finland. What is the role of vocational education? 
* What are the jobs position that this qualification opens?
* What is the structure of the course?
* What is competence based-learning? How competence-based learning will be assessed in the context of this course?
* What is reflecting? What is the role of reflection for the assessment of the course?

_26th August_

* Clarification of basic concepts: pedagogy, working life related learning, vocational pedagogy, didactics ...
* What is cooperative learning?
* Understanding Jig Saw methodology. Hands-on activity in groups: teach others
* Information about Studies in Educational Science course
* How to monitor and document the learning process?

_27th August_ 

* Online tools. 
* PSP and OSSI. Courses in the program. 
* Digital Badges.
* Main online tools: Teams, Drive, Blogger, Wordpress. 

_01st September_
* Working online. Google Drive and Teams. 

## Main outcomes and reflection
 In the [following post]({{< relref "003-startupdays" >}}), I will go through the main learnings and other reflections of the first days.

