---
title: "Pedagogical Competence. Session II. Implementation"
date: 2024-12-17T23:24:46+03:00
draft: true
image: "/images/post/005-implementation.jpg"
author: "Iván Sánchez Milara"
description: ""
categories: ["Reflection", "Seminar","Pedagogical Competence I"]
type: "post"
---

## Introduction

Test

## Main learnings

### Pedagogical Models and learning theories

### Teaching methodologies and personalized path

### Guiding in adult education

## Additional content
[Podcast with a discussion on different perspectives on guiding in higher education](/files/guiding_podcast.mp4). Tanesh Kumar, Afrooz Aminzadeh,  Iván Sánchez. 

[Presentation Pedagogical Models](https://prezi.com/view/nIeIOdE0m5KgKgc5fyej/) Iván Sánchez Milara

## Bibliography

## Footnotes