---
title: "Testing comments"
date: 2021-09-27T20:10:55+03:00
draft: false
author: "Iván Sánchez Milara"
description: "Testing comments in disqus"
categories: ["test, blog"]
type: "post"
---

This page is just to test the comments. Comments are powered by [Disqus](https://disqus.com/). Anonymous comments are supported, just  click on sign up with discuss and mark the tick box at _I'd rather post as a guest_

It is the first time that I use disqus and I do not like that much, is too complex and rather intrusive I think (at least the first impression), but right now is the quickest option. 