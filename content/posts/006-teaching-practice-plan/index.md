---
title: "Teaching practice"
date: 2024-04-17T08:24:47+03:00
draft: false
author: "Iván Sánchez Milara"
description: "Placeholder for the teaching practice preparation"
categories: ["Practice", "Plan","Principles of digital fabrication"]
type: "post"
---

## Introduction

I have done my teaching practice in a course that I have been teaching for several years in the [Faculty of Information Technology and Electrical Engineering](https://www.oulu.fi/en/university/faculties-and-units/faculty-information-technology-and-electrical-engineering) at the [University of Oulu](https://www.oulu.fi/en). The course is a Bachelor level course named *[Principles of Digital Fabrication](https://opas.peppi.oulu.fi/en/course/521159P/7442?period=2023-2024)*

## Documentation

* [Introduction and Lesson Plan](/posts/006-teaching-practice-plan/files/Digipeda_TeachingPractice_SanchezMilara_final.pdf)

