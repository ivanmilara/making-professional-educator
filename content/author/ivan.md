+++
title = "Iván Sánchez Milara"
image = "/images/author/ivan.jpg"
description = "Researcher, teacher and Fab Lab Instructor"
aliases = ["/author/iván-sánchez-milara"]
+++

I am a researcher and teacher at Center for Ubiquitous Computing (University of Oulu) and Fab Lab Instructor at Fab Lab Oulu. I come originally from Spain, but I have lived for long time in Finland. When I have time, I tend to do researcher on educational technology and digital fabrication. I am studying how new technologies can assist primary school teachers into their dailay tasks, and what kind of training teachers are demading.  I have been teaching for more than 14 years different university level courses, and since 2017 I am also organizing workshops and courses on digital fabrication at Fab Lab Oulu.

In this blog I will document my process to become qualified as a professional educator by the [Schools of Professional Teacher Education](https://www.oamk.fi/opinto-opas/en/school-professional-teacher-education) at [OAMK](https://www.oamk.fi/en). I called the blog **Making a professional educator**, because have taken a lot of the Maker culture in my education. I like to *tinker* and explore different methodologies and discuss and share learnings with other colleagues. 

I have intuition that is going to be a funny experience.

Hope it is also of your interest!!!! 